package desafio;

import java.util.Map;
import org.apache.commons.cli.*;

public class Desafio {
    public static void main(String... args) {
        /*
         * Inicializa o cli
         */
        Options options = new Options();
        options.addOption(new Option("v", "valor", true, "Valor que deseja sacar"));
        options.addOption(new Option("s", "saldo", true, "Saldo disponivel no caixa"));

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine commandline;

        int saldo = 300;
        int valor = 0;

        try {
            /**
             * Tratamento de linha de comando
             */
             commandline = parser.parse(options, args);

            if (commandline.hasOption("saldo")) {
                saldo = Integer.parseInt(commandline.getOptionValue("saldo"));
            }

            if (commandline.hasOption("valor")) {
                valor = Integer.parseInt(commandline.getOptionValue("valor"));
            } else {
                mostraHelp(formatter, options);
                System.exit(1);
            }

            /**
             * Operação principal
             */
            CaixaEletronico caixa = new CaixaEletronico(saldo);

            Map<String, Integer> dic = caixa.saque(valor);

            /**
             * Exibição
             */
            String plural;
            for (Map.Entry<String, Integer> entry : dic.entrySet()) {

                plural = entry.getValue() > 1 ? "s" : "";

                System.out.println(
                    entry.getValue().toString() + " cédula" + plural + " de " + entry.getKey()
                );
            }

        } catch (ParseException e) {
            mostraHelp(formatter, options);
        } catch (IllegalArgumentException e) {
            System.out.println("Erro: " + e.getMessage());
        }
        System.exit(1);

    }

    static void mostraHelp(HelpFormatter formatter, Options options) {
        formatter.printHelp(" ", options);
    }

}