# GNU Make 4.1
CLASSPATH := $(shell echo ./lib/*.jar | tr ' ' ':'):.
JAVAC := $(shell which javac)
JAVA := $(shell which java)
build:
	$(JAVAC) -cp $(CLASSPATH) desafio/Desafio.java

test:
	$(JAVAC) -cp $(CLASSPATH) desafio/CaixaEletronicoTest.java
	$(JAVA) -cp $(CLASSPATH) org.junit.runner.JUnitCore desafio.CaixaEletronicoTest

clean:
	$(RM) desafio/*.class

all: build test

.PHONY: all build test run clean
