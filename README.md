# Desafio - Caixa Eletrônico

http://dojopuzzles.com/problemas/exibe/caixa-eletronico/

## O Problema
_Desenvolva um programa que simule a entrega de notas quando um cliente efetuar um saque em um caixa eletrônico. Os requisitos básicos são os seguintes:_
- _Entregar o menor número de notas;_
- _É possível sacar o valor solicitado com as notas disponíveis_
- _Saldo do cliente infinito;_
- _Quantidade de notas infinito (pode-se colocar um valor finito de cédulas para aumentar a dificuldade do problema);_
- _Notas disponíveis de R$ 100,00; R$ 50,00; R$ 20,00 e R$ 10,00_

Exemplos:
- _Valor do Saque: R$ 30,00 – Resultado Esperado: Entregar 1 nota de R$20,00 e 1 nota de R$ 10,00._
- _Valor do Saque: R$ 80,00 – Resultado Esperado: Entregar 1 nota de R$50,00 1 nota de R$ 20,00 e 1 nota de R$ 10,00._

## A Solução
Foi implementada uma classe `CaixaEletronico`, invocada a partir do método `main` dentro de `Desafio.java`. O objetivo é deixar o código bem simples e intencional. Foi adicionado um limite máximo para saque, configurável por linha de comando (assim como a quantia que se deseja sacar). Ficou assim:

```
$ ./run
usage:
 -s,--saldo <arg>   Saldo disponivel no caixa
 -v,--valor <arg>   Valor que deseja sacar
```
Por exemplo: Para sacar R$180 (uma quantia interessante, que precisa de cada um dos 4 tipos de cédulas), basta executar:
```
$ ./run -v 180
1 cédula de R$10
1 cédula de R$20
1 cédula de R$50
1 cédula de R$100
````

Caso o saldo total não seja informado através da opção `-s`, este fica em R$ 300 por _default_.

O algoritmo é o mais simples possível:
- Seja _s_ o valor solicitado para o saque;
- Seja _t_ o saldo total;
- Se _s_ > _t_, o saque não é possível.
- Seja _v_ o valor da maior cédula que atende este saque;
- Enquanto _s_ for maior ou igual a _v_
    - Separe uma cédula de valor _v_
    - _s_ = _s_ - _v_
- Ao final, devolva as cédulas separadas

(Suportaria trivialmente notas de R$ 5)

A versão utilizada foi o OpenJDK 11, conforme abaixo:
```
#java -version
openjdk version "11.0.6" 2020-01-14
OpenJDK Runtime Environment (build 11.0.6+10-post-Ubuntu-1ubuntu118.04.1)
OpenJDK 64-Bit Server VM (build 11.0.610-post-Ubuntu-1ubuntu118.04.1, mixed mode, sharing)
```

## Build / run / test
Para compilar o programa e os testes foi usado o `GNU Make`. Ele define os seguintes _targets_: `clean` (remove os arquivos `.class`), `build` (compila a aplicação) e `test` (compila e executa os testes).

Também foi incluido um shell script `run`, que nada mais faz do que expandir o _classpath_ ~~através de um hack xexelento~~ e invocar o `java`.

```bash
#!/bin/sh
java -cp $(echo lib/*.jar | tr ' ' ':'):. desafio.Desafio $1 $2
```

O diretório `lib` contém as dependencias do projeto: **Apache Common CLI**, **jUnit** e **Hamcrest Core**. Os arquivos `.jar` foram incluidos para garantir a versão destes componentes.


## Testes
Foram implementados com `junit`. A classe `CaixaEletronicoTest` contém os testes. basta invocar `make test` para compilar e executar os mesmos.

```
$ make test
/usr/bin/javac -cp ./lib/commons-cli-1.4.jar:./lib/hamcrest-core-1.3.jar:./lib/junit-4.13.jar:. desafio/CaixaEletronicoTest.java
/usr/bin/java -cp ./lib/commons-cli-1.4.jar:./lib/hamcrest-core-1.3.jar:./lib/junit-4.13.jar:. org.junit.runner.JUnitCore desafio.CaixaEletronicoTest
JUnit version 4.13
.....
Time: 0,019

OK (5 tests)
```




