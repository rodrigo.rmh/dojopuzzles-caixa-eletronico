package desafio;

import java.util.HashMap;
import java.util.Map;

public class CaixaEletronico {

    /**
     * Valores de cédula suportados pelo caixa
     */
     private int[] cedulas = { 100, 50, 20, 10 };

    /** 
     * Saldo atual do caixa
    */
     private int saldo;

    public CaixaEletronico(int saldo) {
        if (saldo < 0)
            throw new IllegalArgumentException("O saldo informado é negativo");
        if (saldo == 0)
            throw new IllegalArgumentException("O saldo informado é insuficiente");
        // if ((saldo % 10) != 0)
        // throw new IllegalArgumentException("saldo quebrado");

        this.saldo = saldo;
    }

    public CaixaEletronico(String saldo) {
        this(Integer.parseInt(saldo));
    }

    public CaixaEletronico() {
        this(1000);
    }

    public int getSaldo() {
        return this.saldo;
    }
    
    /**
     * Tenta sacar uma quantia do caixa
     * @param valor quantidade que se deseja sacar
     * @return Map<String,Integer> com a cédula + quantidade
     */
    public Map<String, Integer> saque(int valor) {

        if (valor > this.saldo)
            throw new IllegalArgumentException("Caixa não possui fundos para efetuar a operação");

        if ((valor % 10) != 0)
            throw new IllegalArgumentException("Não é possível sacar o valor solicitado com as cédulas disponíveis");

        Map<String, Integer> d = new HashMap<String, Integer>();

        // chave do Map
        String chave = "";

        for (int i = 0; i < cedulas.length; i++) {
            while (valor >= cedulas[i]) {
                valor -= cedulas[i];
                chave = "R$" + cedulas[i];
                if (d.get(chave) == null)
                    d.put(chave, 0);
                d.put(chave, d.get(chave) + 1);
            }
        }
        return d;
    }

    public Map<String, Integer> saque(String valor) {
        return this.saque(Integer.parseInt(valor));
    }

}