package desafio;

import org.junit.*;
import static org.junit.Assert.*;

import java.util.Map;
import java.util.stream.IntStream;

public class CaixaEletronicoTest {
	private CaixaEletronico caixa;

	@Test
	public void utiliza_todos_os_tipos_de_cedulas_quando_possivel() {
		caixa = new CaixaEletronico();
		Map<String, Integer> uns_180_pilas = Map.of("R$100", 1, "R$50", 1, "R$20", 1, "R$10", 1);
		assertEquals("Saque esperando todos os tipos de cedulas", uns_180_pilas, caixa.saque(180));
	}

	@Test
	public void saca_corretamente_valores_diversos(){
		caixa = new CaixaEletronico();
		assertEquals("Saca R$10 com uma única cédula",  caixa.saque(10),  Map.of("R$10",  1));
		assertEquals("Saca R$20 com uma única cédula",  caixa.saque(20),  Map.of("R$20",  1));
		assertEquals("Saca R$50 com uma única cédula",  caixa.saque(50),  Map.of("R$50",  1));
		assertEquals("Saca R$100 com uma única cédula", caixa.saque(100), Map.of("R$100", 1));
		
		assertEquals("Saca R$30 corretamente", caixa.saque(30), Map.of("R$20", 1, "R$10", 1));
		assertEquals("Saca R$40 corretamente", caixa.saque(40), Map.of("R$20", 2));
		assertEquals("Saca R$60 corretamente", caixa.saque(60), Map.of("R$50", 1, "R$10", 1));
		assertEquals("Saca R$70 corretamente", caixa.saque(70), Map.of("R$50", 1, "R$20", 1));
		assertEquals("Saca R$80 corretamente", caixa.saque(80), Map.of("R$50", 1, "R$20", 1, "R$10", 1));

		assertEquals("Saca R$500 corretamente",  caixa.saque(500),  Map.of("R$100", 5));

	}

	@Test
	public void mostra_apenas_os_tipos_utilizados() {
		caixa = new CaixaEletronico();
		Map<String, Integer> mil = Map.of("R$100", 10);
		assertEquals("Saque esperando um unico tipo de cedula", mil, caixa.saque(1000));
	}

	@Test(expected = IllegalArgumentException.class)
	public void saldo_zerado_throws_IllegalArgumentException() {
		caixa = new CaixaEletronico(0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void saldo_negativo_throws_IllegalArgumentException() {
		caixa = new CaixaEletronico(-1);
	}

	// @Test(expected = IllegalArgumentException.class)
	// public void caixa_nao_funciona_com_valores_quebrados(){
	// caixa = new CaixaEletronico(77);
	// }

	@Test(expected = IllegalArgumentException.class)
	public void caixa_nao_permite_sacar_mais_do_que_o_saldo() {
		caixa = new CaixaEletronico(100);
		caixa.saque(200);
	}

}